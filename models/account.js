const bcrypt = require('bcryptjs');
module.exports = (sequelize, DataTypes) => {
    var Account = sequelize.define('account', {
        accountId: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        name: {
            type: DataTypes.STRING,
        },
        age: {
            type: DataTypes.STRING,
        },
        email: {
            type: DataTypes.STRING,
            required: true,
            allowNull: false,
            unique: {
                args: true,
                msg: 'Email address already in use!'
            },
            validate: {
                isEmail: { msg: 'Email id not valid' }
            }
        },
        password: {
            type: DataTypes.STRING,
        },
        gender: {
            type: DataTypes.ENUM,
            values: ['male', 'female', 'other'],
            required: true
        },
        role: {
            type: DataTypes.ENUM,
            values: ['coordinator', 'respondent'],
            required: true
        },
    }, {
        timestamps: true
    });

   // Account.beforeSave(async (user) => {
   //     if (user.changed('password')) {
    //        let hash = bcrypt.hashSync(user.password, bcrypt.genSaltSync(5));
     //       user.password = hash;
      //  }
   // });

    return Account;
}
