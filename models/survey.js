module.exports = (sequelize, DataTypes) => {
    var Survey = sequelize.define("survey", {
        surveyId: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        surveyName: {
            type: DataTypes.STRING,
            required: true
        },
        expiry: {
            type: DataTypes.DATE,
            required: true
        },
        gender: {
            type: DataTypes.ENUM,
            values: ['male', 'female', 'other', 'all'],
            required: true
        },
        ageGroup: {
            type: DataTypes.STRING,
            required: true
        }
    }, {
        timestamps: true
    });

    Survey.sync({
        alter: true
    }).then(() => {
        //console.log('New table created');
    });
    return Survey;
}
