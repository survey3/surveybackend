module.exports = (sequelize, DataTypes) => {
    var Responses = sequelize.define("responses", {
        responseId: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        surveyId: {
            type: DataTypes.UUID,
            required: true
        },
        accountId: {
            type: DataTypes.UUID,
            required: true
        },
        questionId: {
            type: DataTypes.UUID,
            required: true
        },
        answer: {
            type: DataTypes.STRING,
            required: true,
        }
    }, {
        timestamps: true
    });

    Responses.sync({ alter: true }).then(() => { }).catch((err) => { });

    return Responses;
}