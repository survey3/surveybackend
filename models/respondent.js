module.exports = (sequelize, DataTypes) => {
    var Respondent = sequelize.define('respondent', {
        respondentId: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        accountId: {
            type: DataTypes.UUID,
            required: true
        },
        surveyId: {
            type: DataTypes.UUID,
            required: true
        },
        hasResponded: {
            type: DataTypes.BOOLEAN,
            defaultValue: '0'
        }
    });

    Respondent.sync({ alter: true }).then(() => { });
    return Respondent;
}