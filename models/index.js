'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

// model imports
db.account = require('./account')(sequelize, Sequelize);
db.survey = require('./survey')(sequelize, Sequelize);
db.respondent = require('./respondent')(sequelize, Sequelize);
db.question = require('./question')(sequelize, Sequelize);
db.responses = require('./responses')(sequelize, Sequelize);

// associations

db.account.hasMany(db.respondent, {
  onDelete: 'CASCADE',
  hooks: true,
  foreignKey: {
    name: 'accountId',
    allowNull: false
  }
});

db.respondent.belongsTo(db.survey, {
  onDelete: 'CASCADE',
  hooks: true,
  foreignKey: {
    name: 'surveyId',
    allowNull: false
  }
});

db.survey.hasMany(db.question, {
  onDelete: 'CASCADE',
  hooks: true,
  foreignKey: {
    name: 'surveyId',
    allowNull: false
  }
});



// db.survey.belongsTo(db.respondent, {
//   onDelete: 'CASCADE',
//   hooks: true,
//   foreignKey: {
//     name: 'surveyId',
//     allowNull: false
//   }
// });


// responses

db.responses.hasMany(db.survey, {
  onDelete: 'CASCADE',
  hooks: true,
  foreignKey: {
    name: 'surveyId',
    allowNull: false
  }
});

db.responses.hasMany(db.respondent, {
  onDelete: 'CASCADE',
  hooks: true,
  foreignKey: {
    name: 'surveyId',
    allowNull: false
  }
});


// Sync db changes
sequelize.sync({ alter: true });

module.exports = db;