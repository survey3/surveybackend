module.exports = (sequelize, DataTypes) => {
    var Question = sequelize.define("question", {
        questionId: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4
        },
        question: {
            type: DataTypes.STRING,
            required: true
        },
        type: {
            type: DataTypes.ENUM,
            values: ['text', 'radio', 'checkbox'],
            required: true
        },
        options: {
            type: DataTypes.STRING,
        },
        surveyId: {
            type: DataTypes.UUID,
            required: true
        }
    }, {
        timestamps: true
    });


    Question.sync({
        alter: true
    }).then(() => {
        //console.log('New table created');
    });
    return Question;
}