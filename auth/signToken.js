const JWT = require('jsonwebtoken');
const keys = require('../config/keys');
signToken = (id) => {
    return JWT.sign({
        iss: keys.secrets.JWT_ISSUER,
        sub: id,
    }, keys.secrets.JWT_SECRET)
}
module.exports = signToken;