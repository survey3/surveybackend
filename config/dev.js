const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
module.exports = {
    MySQL: {
        "path": 'mysql://' + config.username + ':' + encodeURIComponent(config.password) + `@${config.host}:3306/${config.database}`
    },

    secrets: {
        JWT_SECRET: "secretKey",
        JWT_ISSUER: 'SurveyApp'
    }
};