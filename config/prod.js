const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
module.exports = {
    MySQL: {
        'path': `mysql://b79782357fe5ad:b39c2676@us-cdbr-east-05.cleardb.net/heroku_90f7c27c40ba3ee?reconnect=true`
    },

    secrets: {
        JWT_SECRET: "secretKey",
        JWT_ISSUER: 'SurveyApp'
    },

}
    // USERNAME:PASSWORD@DB_HOST/DB_NAME