const db = require('../models/index');
const { Op } = require("sequelize");

module.exports = {
    addSurvey: async (req, res, next) => {

        const { surveyName: $surveyName, ageGroup: $ageGroup, gender: $gender, expiry: $expiry, questions: $questions } = req.body;
        const age_lower_lim = $ageGroup.split(',')[0];
        const age_upper_lim = $ageGroup.split(',')[1];

        db.survey.create({
            surveyName: $surveyName, ageGroup: $ageGroup, gender: $gender, expiry: new Date($expiry),
        }).then((survey) => {
            // add survey Id in questionMapping table
            $questions.map((ele) => {
                ele.surveyId = survey.surveyId;
                if (ele.options !== null) {
                    ele.options = ele.options.join('|');
                }
            });
            db.question.bulkCreate($questions)
                .then((question) => {
                    db.account.findAll({
                        attributes: ['accountId'],
                        where: {
                            age: {
                                [Op.between]: [age_lower_lim, age_upper_lim]
                            },
                            role: 'respondent',
                            gender: $gender,
                        },
                        raw: true
                    }).then((elligible) => {
                        elligible.map(ele => ele['surveyId'] = survey.surveyId);
                        console.log(elligible);
                        db.respondent.bulkCreate(elligible)
                            .then((result) => {
                                res.status(200).json({
                                    status: true,
                                    message: 'success',
                                    data: {}
                                });
                            })
                            .catch((err) => {
                                res.status(500).json({
                                    status: false,
                                    message: err,
                                    data: {}
                                });
                            })
                    })
                }).catch((err) => {
                    res.status(500).json({
                        status: false,
                        message: err,
                        data: {}
                    });
                })
                .catch((err) => {
                    res.status(500).json({
                        status: false,
                        message: err,
                        data: {}
                    });
                });

        }).catch((err) => {
            res.status(500).json({
                status: false,
                message: err,
                data: {}
            });
        });


    },

    getAllSurveys: async (req, res, next) => {

        db.survey.findAll({
            include: [{ model: db.question }]
        }).then((result) => {
            res.status(200).json({
                status: true,
                message: 'success',
                data: result
            });
        }).catch((err) => {
            res.status(500).json({
                status: false,
                message: err,
                data: {}
            });
        });
    },


    getSurveysById: async (req, res, next) => {
        const { accountId: $accountId } = req.body;
        console.log($accountId);
        db.respondent.findAll({
            where: { accountId: $accountId },
            include: {
                model: db.survey,
                where: {
                    expiry: {
                        // yyyy-mm-dd
                        [Op.gte]: new Date()
                    }
                },
                include: {
                    model: db.question,
                    attributes: {
                        exclude: ['createdAt', 'updatedAt']
                    }
                },
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                }
            },
            attributes: ['respondentId', 'surveyId', 'accountId', 'hasResponded'],
        },
        )
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: 'success',
                    data: result
                });
            })
            .catch((err) => {
                res.status(500).json({
                    status: false,
                    message: err,
                    data: {}
                });
            })


        // db.survey.findAll({
        //     include: [{ model: db.question }]
        // }).then((result) => {
        //     res.status(200).json({
        //         status: true,
        //         message: 'success',
        //         data: result
        //     });
        // }).catch((err) => {
        //     res.status(500).json({
        //         status: false,
        //         message: err,
        //         data: {}
        //     });
        // });
    },

    respond: async (req, res, next) => {

        const $accountId = req.body[0].accountId;

        db.responses.bulkCreate(req.body)
            .then((result) => {
                db.respondent.update({
                    hasResponded: true
                }, {
                    where: {
                        accountId: $accountId
                    }
                })
                    .then((result) => {
                        res.status(200).json({
                            status: true,
                            message: 'success',
                            data: {}
                        });
                    })
                    .catch((err) => {
                        res.status(500).json({
                            status: false,
                            message: err,
                            data: {}
                        });
                    });
            })
            .catch((err) => {
                res.status(500).json({
                    status: false,
                    message: err,
                    data: {}
                });
            });

    }
}