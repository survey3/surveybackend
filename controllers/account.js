const db = require('../models/index');
const jwt = require('jsonwebtoken');
const findUserFromToken = require('../helper/findUserFromToken');
const signToken = require('../auth/signToken');

module.exports = {

    getAccountInfo: async (req, res, next) => {

        const dataUser = req.user;
        const token = req.headers.authorization;
        var user = await findUserFromToken(token);
        res.status(200).json({
            status: true,
            message: "success",
            data: user
        })
    },


    register: async (req, res, next) => {
        const { name: $name, email: $email, age: $age, gender: $gender, password: $password, role: $role } = req.body;

        db.account.create({
            name: $name, email: $email, age: $age, gender: $gender, password: $password, role: $role
        })
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: 'success',
                    data: {}
                });
            })
            .catch((err) => {
                res.status(500).json({
                    status: false,
                    message: err.message,
                    data: {}
                });
            })
    },

    login: async (req, res, next) => {
        const { email: $email, password: $password } = req.body;

        const acccountDetails = await db.account.findOne({
            where: { email: $email, password: $password }
        }).catch((err) => {
            res.status(500).json({
                status: false,
                message: err,
                data: {}
            });
        });

        //     let { dataValues } = result;
        //     let { id } = dataValues;

        if (acccountDetails === null) {
            res.status(500).json({
                status: false,
                message: 'User not registered',
                data: {}
            });
        }


        const token = await signToken(acccountDetails['accountId']);

        res.status(200).json({
            status: true,
            message: "success",
            data: token
        });

        // .then((result) => {

        //     console.log(id);
        //     jwt.sign({ id }, 'secretKey', {}, (err, token) => {
        //         res.status(200).json({
        //             status: true,
        //             token: token,
        //             message: 'Logged in successfully.',
        //             data: result
        //         });
        //     });

        // })


    }
}
