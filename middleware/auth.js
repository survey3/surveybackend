const jwt = require('jsonwebtoken');
const db = require('../models/index');


module.exports = {
    auth: async (req, res, next) => {
        const token = req.header('Authorization');
        // check for token
        if (!token) res.status(401).json({ message: 'Unauthorized user' });
        try {
            // verify token
            const decoded = jwt.verify(token, 'secretKey');
            // add user from payload
            db.account.findOne({
                where: {
                    accountId: decoded['sub']
                },
                attributes: {
                    exclude: ['password', 'createdAt', 'updatedAt']
                }
            }).then(found => {
                let { dataValues } = found;
                let x = { accountId, name, email } = dataValues;
                req.user = x;
            }).catch((err) => {
            })
            next();
        } catch (e) {
            res.status(400).json({ message: 'Token is not valid' })
        }
    }
}