const jwtDecode = require('jwt-decode');
const db = require('../models/index');
findUserFromToken = async (token) => {
    var decoded = jwtDecode(token);
    const id = decoded.sub;
    var foundUser = await db.account.findOne({
        where: {
            accountId: id
        }
    });
    return foundUser;
};

module.exports = findUserFromToken;