
const express = require('express');
const router = express.Router();
const surveyController = require('../controllers/survey');
const authGuard = require('../middleware/auth');


router.route('/addSurvey')
    .post(authGuard.auth, surveyController.addSurvey);

router.route('/getAllSurveys')
    .get(authGuard.auth, surveyController.getAllSurveys);

router.route('/getSurveysById')
    .post(authGuard.auth, surveyController.getSurveysById);

router.route('/respond')
    .post(authGuard.auth, surveyController.respond);


module.exports = router;