const express = require('express');
const router = express.Router();
const accountController = require('../controllers/account');

router.route('/register')
    .post(accountController.register);

router.route('/login')
    .post(accountController.login);

router.route('/getAccountInfo')
    .get(accountController.getAccountInfo);

module.exports = router;